<?php
 include("source/login.php");
$p = new users();
session_start();
?>
<!DOCTYPE html>
<html lang="vn">
    <head>
    <meta charset="UTF-8">
   <link rel="stylesheet" href="css/edit.css">
   <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/varkala/1-0/vendor/aos/aos.css">
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/varkala/1-0/css/style.default.19ba8987.css" id="theme-stylesheet">
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/varkala/1-0/css/custom.0a822280.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
    <script src="jquery/dist/jquery.slim.min.js"></script>
    <script src="jquery/dist/jquery.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    </head>
    <body>
    <!--header-->
    <?php include('header.php');?>
     
    <!--end header -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="images/slider1.jpg" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="images/dog-4.jpg" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="images/slider3.jpg" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    <!--end-->
      <div class="bg-gray-100 position-relative">
          <div class="container py-6">
            <div class="row">
              <div class="col-sm-6 mb-5 mb-sm-0">
                <div class="card card-scale shadow-0 border-0 text-white text-hover-gray-900 overlay-hover-light text-center"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/category-women.c3895b68.jpg" alt="Card image" class="card-img img-scale">
                  <div class="card-img-overlay d-flex align-items-center"> 
                    
                  </div>
                </div>
              </div>
              <div class="col-sm-6 mb-5 mb-sm-0">
                <div class="card card-scale shadow-0 border-0 text-white text-hover-gray-900 overlay-hover-light text-center"><img src="images/boo.jpg" alt="Card image" class="card-img img-scale">
                  <div class="card-img-overlay d-flex align-items-center"> 
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid px-lg-5 px-xl-7 py-6">
            <div class="row">
              <div class="col-lg-10 col-xl-8 text-center mx-auto">
                <h2 class="display-3 mb-5">New Arrivals</h2>
                <p class="lead text-muted mb-6">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections</p>
              </div>
            </div>
            <div class="row justify-content-between align-items-center mb-4">
              <div class="col-12 col-sm">
                <ul class="list-inline text-center text-sm-left mb-3 mb-sm-0"> 
                  <li class="list-inline-item"><a href="#" class="text-dark">All Products </a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Clothing </a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Bags</a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Shoes</a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Accessories</a></li>
                </ul>
              </div>
              <div class="col-12 col-sm-auto text-center"><a href="#" class="btn btn-link px-0">All products</a></div>
            </div>
            <div class="row">
              <!-- product-->
              <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                <div class="product aos-init aos-animate" data-aos="zoom-in" data-aos-delay="0">
                  <div class="product-image mb-md-3">
                    <div class="product-badge badge badge-secondary">Fresh</div><a href="detail-1.html">
                      <div class="product-swap-image">
                          <img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/0987188250_1_1_1.43f81a4a.jpg" alt="product" class="img-fluid product-swap-image-front"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/0987188250_2_1_1.7bbd2efc.jpg" alt="product" class="img-fluid"></div></a>
                          <div class="product-hover-overlay">
                            <a href="#" class="text-dark text-sm">
                              <svg class="svg-icon text-hover-primary svg-icon-heavy d-sm-none">
                                <use xlink:href="#retail-bag-1"> </use>
                              </svg>
                              <span class="d-none d-sm-inline">Add to cart</span>
                            </a>
                          <div>
                          <a href="#" class="text-dark text-hover-primary mr-2">
                            <svg class="svg-icon svg-icon-heavy">
                              <use xlink:href="#heart-1"> </use>
                          </svg>
                          </a>
                        <a href="#" data-toggle="modal" data-target="#quickView" class="text-dark text-hover-primary text-decoration-none">
                          <svg class="svg-icon svg-icon-heavy">
                            <use xlink:href="#expand-1"> </use>
                          </svg>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="position-relative">
                    <h3 class="text-base mb-1"><a href="detail-1.html" class="text-dark">White Tee</a></h3><span class="text-gray-500 text-sm">$40.00</span>
                    <div class="product-stars text-xs"><i class="fa fa-star text-primary"></i><i class="fa fa-star text-primary"></i><i class="fa fa-star text-primary"></i><i class="fa fa-star text-muted"></i><i class="fa fa-star text-muted"></i></div>
                  </div>
                </div>
              </div>
              <!-- /product   -->
            </div>
            <!-- Quickview Modal    -->
            <div id="quickView" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade quickview" style="">
              <div role="document" class="modal-dialog modal-xl">
                <div class="modal-content">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close close-absolute close-rotate">
                    <svg class="svg-icon w-3rem h-3rem svg-icon-light align-middle">
                      <use xlink:href="#close-1"> </use>
                    </svg>
                  </button>
                  <div class="modal-body quickview-body">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="detail-carousel">
                          <div class="product-badge badge badge-primary">Fresh</div>
                          <div class="product-badge badge badge-dark">Sale</div>
                          <div id="quickViewSlider" class="swiper-container quickview-slider">
                            <!-- Additional required wrapper-->
                            <div class="swiper-wrapper">
                              <!-- Slides-->
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-1-gray.1453e467.jpg" alt="Modern Jacket 1" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-2-gray.e4eff43a.jpg" alt="Modern Jacket 2" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-3-gray.21f669db.jpg" alt="Modern Jacket 3" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-4-gray.f1e52d09.jpg" alt="Modern Jacket 4" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-5-gray.b7b1581e.jpg" alt="Modern Jacket 5" class="img-fluid"></div>
                            </div>
                          </div>
                          <div data-swiper="#quickViewSlider" class="swiper-thumbs">
                            <button class="swiper-thumb-item detail-thumb-item mb-3 active"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-1-gray.1453e467.jpg" alt="Modern Jacket 0" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-2-gray.e4eff43a.jpg" alt="Modern Jacket 1" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-3-gray.21f669db.jpg" alt="Modern Jacket 2" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-4-gray.f1e52d09.jpg" alt="Modern Jacket 3" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-5-gray.b7b1581e.jpg" alt="Modern Jacket 4" class="img-fluid"></button>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6 p-lg-5">
                        <h2 class="mb-4 mt-4 mt-lg-1">Push-up Jeans</h2>
                        <div class="d-flex flex-column flex-sm-row align-items-sm-center justify-content-sm-between mb-4">
                          <ul class="list-inline mb-2 mb-sm-0">
                            <li class="list-inline-item h4 font-weight-light mb-0">$65.00</li>
                            <li class="list-inline-item text-muted font-weight-light"> 
                              <del>$90.00</del>
                            </li>
                          </ul>
                          <div class="d-flex align-items-center text-sm">
                            <ul class="list-inline mr-2 mb-0">
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-gray-300"></i></li>
                            </ul><span class="text-muted text-uppercase">25 reviews</span>
                          </div>
                        </div>
                        <p class="mb-4 text-muted">Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.</p>
                        <form id="buyForm_modal" action="#">
                          <div class="row">
                            <div class="col-sm-6 col-lg-12 detail-option mb-4">
                              <h6 class="detail-option-heading">Size <span>(required)</span></h6>
                              <div class="dropdown bootstrap-select"><select name="size" data-style="btn-selectpicker" class="selectpicker" tabindex="-98">
                                <option value="value_0">Small</option>
                                <option value="value_1">Medium</option>
                                <option value="value_2">Large</option>
                              </select><button type="button" class="btn dropdown-toggle btn-selectpicker" data-toggle="dropdown" role="combobox" aria-owns="bs-select-1" aria-haspopup="listbox" aria-expanded="false" title="Small"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner">Small</div></div> </div></button><div class="dropdown-menu "><div class="inner show" role="listbox" id="bs-select-1" tabindex="-1"><ul class="dropdown-menu inner show" role="presentation"></ul></div></div></div>
                            </div>
                            <div class="col-sm-6 col-lg-12 detail-option mb-5">
                              <h6 class="detail-option-heading">Type <span>(required)</span></h6>
                              <label for="material_0_modal" class="btn btn-sm btn-outline-primary detail-option-btn-label">
                                  
                                Hoodie
                                <input type="radio" name="material" value="value_0" id="material_0_modal" required="" class="input-invisible">
                              </label>
                              <label for="material_1_modal" class="btn btn-sm btn-outline-primary detail-option-btn-label">
                                  
                                College
                                <input type="radio" name="material" value="value_1" id="material_1_modal" required="" class="input-invisible">
                              </label>
                            </div>
                          </div>
                          <div class="input-group w-100 mb-4">
                            <input name="items" type="number" value="1" class="form-control form-control-lg detail-quantity">
                            <div class="input-group-append flex-grow-1">
                              <button type="submit" class="btn btn-dark btn-block"> <i class="fa fa-shopping-cart mr-2"></i>Add to Cart</button>
                            </div>
                          </div>
                          <div class="row mb-4">
                            <div class="col-6"><a href="#"> <i class="far fa-heart mr-2"></i>Add to wishlist </a></div>
                            <div class="col-6 text-right">
                              <ul class="list-inline mb-0">
                                <li class="list-inline-item mr-2"><a href="#" class="text-dark text-hover-primary"><i class="fab fa-facebook-f"> </i></a></li>
                                <li class="list-inline-item"><a href="#" class="text-dark text-hover-primary"><i class="fab fa-twitter"> </i></a></li>
                              </ul>
                            </div>
                          </div>
                          <ul class="list-unstyled">
                            <li><strong>Category:</strong> <a href="#" class="text-muted">Jeans</a></li>
                            <li><strong>Tags:</strong> <a href="#" class="text-muted">Leisure</a>, <a href="#" class="text-muted">Elegant</a></li>
                          </ul>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container py-6">
              <h5 class="text-uppercase text-primary letter-spacing-3 mb-3">Our History</h5>
              <p class="lead text-muted mb-4">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections </p>
              <p class="lead text-muted mb-5">He must have tried it a hundred times, shut his eyes so that he wouldn't have to look at the floundering legs, and only stopped when he began to feel a mild, dull pain there that he had never felt before.     </p>
            </div>
              <!---footer-->
              <?= include("footer.php"); ?>
              <!--end footer-->
                 <!--modal sign up-->
                 <div class="modal fade" id="signupModal" tabindex="0" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title h2 font-weight-bold" id="exampleModalLongTitle">Sign up </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="" id="signup" method="post">
                        <div class="form-group">
                          <label for="fname">FirstName:</label>
                          <input name="fname" id="firstname" class="form-control" placeholder="First name" type="text"/>
                        </div>
                        <div class="form-group">
                          <label for="lname">LastName:</label>
                          <input name="lname" id="lname" class="form-control" placeholder="Last name" type="text"/>
                        </div>
                        <div class="form-group"> 
                          <label for="phone">Phone:</label>
                          <input name="phone" id="phone" class="form-control" placeholder="Enter username" type="text" required/>
                        </div>
                        <div class="form-group">
                          <label for="username">Username:</label>
                          <input name="txtuser" id="username" class="form-control" placeholder="Enter username" type="" required/>
                        </div>
                        <div class="form-group">
                          <label for="password">Password:</label>
                          <input name="txtpass" id="password" class="form-control" placeholder="Enter password" type="password" required/>
                        </div>
                        <div class="form-group">
                          <label for="password">Confirm Password:</label>
                          <input name="txt_confirm_pass" id="confirm_pass" class="form-control" placeholder="Enter confirm password" type="password" required/>
                        </div>
                        <div class="form-group">
                          <label for="address">Address:</label>
                          <input name="address" id="address" class="form-control" placeholder="Enter address" type="text"/>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button name="button" type="submit" class="btn btn-primary" value="Sign up">Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>      
              </div>
                <!--end-->
                <!--modal sign in-->
                <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title h2 font-weight-bold" id="exampleModalLongTitle">Sign in </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="" id="signin" method="post">
                          <div class="form-group">
                            <label for="username">Username:</label>
                            <input name="txtuser" id="username" class="form-control" placeholder="Enter username" type="text"/>
                          </div>
                         <div class="form-group">
                            <label for="password">Password:</label>
                            <input name="txtpass" id="password" class="form-control" placeholder="Enter password" type="password"/>
                         </div>
                         <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button name="button" type="submit" class="btn btn-primary" value="Sign in">Sign in</button>
                      </div>
                         <?php
                            $username = $_REQUEST["txtuser"];
                            $password = $_REQUEST["txtpass"];
                            $fname = $_REQUEST["fname"];
                            $lname = $_REQUEST["lname"];
                            $phone =  $_REQUEST["phone"];
                            $address =  $_REQUEST["address"];
                            $txt_confirm_pass =  $_REQUEST["txt_confirm_pass"];
                            switch($_POST["button"]){
                              case "Sign in":
                              {
                                if($username != '' && $password != '')
                                {
                                  $p->dangnhap( $username, $password);
                                }
                                else
                                {
                                  echo '<script>
                                      alert("Username hoặc password không được rỗng")
                                  </script>';
                                  echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"> <strong>Username or password rỗng</strong> <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>';
                                }
                                break;
                              }
                              case "Sign up":
                              {                                                        
                                if($username =='' || $password == '' ||  $txt_confirm_pass == '' ||  $fname =='' || $lname== '' || $phone == '' || $address =='')
                                {   
                                  echo'<script>
                                  alert("Vui lòng điền đủ thông tin");
                                </script>';
                                }
                                else
                                {   $test = 1000;
                                  $test = $p->dangki($username, $password, $phone, $fname, $lname, $txt_confirm_pass, $address);                                                                    
                                  if( $test == 1)
                                  {
                                    echo'<script>
                                      alert("Đăng kí thành công");
                                    </script>';
                                  }
                                 elseif($test == 0)
                                  {
                                    echo'<script>
                                      alert("Username đã tồn tại");
                                    </script>'; 
                                  }
                                  else
                                  {
                                    echo'<script>
                                    alert("Password xác nhận không khớp");
                                  </script>';
                                  }
                                 }
                                 break;
                               }                                                         
                            }
                         ?>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <!--end-->               
    </body>
</html>