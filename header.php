<?php
error_reporting("all");
  include("source/show.php");
  $point = new show_data();
?> 
<header class="header header-absolute"> 
  <!-- Top Bar-->
  <div class="top-bar text-sm bg-transparent">
    <div class="container-fluid px-lg-5 py-3" id="top-header">
      <div class="row align-items-center">
        <div class="col-md-4 d-none d-md-block">
          <ul class="list-inline mb-0">
            <li class="list-inline-item mr-2"><a href="#" class="text-reset text-hover-primary"><i class="fab fa-facebook-f"> </i></a></li>
            <li class="list-inline-item mr-4"><a href="#" class="text-reset text-hover-primary"><i class="fab fa-twitter"> </i></a></li>
            <li class="list-inline-item mr-2">
              <svg class="svg-icon mr-2">
                <use xlink:href="#calls-1"> </use>
              </svg>020-800-456-747
            </li>
          </ul>
        </div>
        <div class="col-sm-6 col-md-4 text-left text-md-center">
          <p class="mb-0">Welcome to Petshome store</p>
        </div>
        <div class="col-sm-6 col-md-4 d-none d-sm-flex justify-content-end">
          <!-- Language Dropdown-->
          <div class="dropdown border-right px-3"><a id="langsDropdown" href="#" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle topbar-link">English</a>
            <div aria-labelledby="langsDropdown" class="dropdown-menu dropdown-menu-right" style="transform: translateY(30px);"><a href="#" class="dropdown-item">German</a><a href="#" class="dropdown-item">French</a></div>
          </div>
          <!-- Currency Dropdown-->
          <div class="dropdown pl-3 ml-0"><a id="currencyDropdown" href="#" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle topbar-link">USD</a>
            <div aria-labelledby="currencyDropdown" class="dropdown-menu dropdown-menu-right" style="transform: translateY(30px);"><a href="#" class="dropdown-item">EUR</a><a href="#" class="dropdown-item"> GBP</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Top Bar End-->
  <nav class="navbar navbar-expand-lg bg-transparent  navbar-light px-lg-5 "><a href="index.php" class="navbar-brand" id="customize-home">Trang Chủ</a>
    <ul class="list-inline mb-0 d-block d-lg-none">
      <li class="list-inline-item mr-3"><a href="#" data-toggle="modal" data-target="#loginModal">
          <svg class="svg-icon navbar-icon">
            <use xlink:href="#avatar-1"> </use>
          </svg></a></li>
          <!--modal sign in-->
          <!-- Button trigger modal -->
          <!--end modal-->
      <li class="list-inline-item mr-3"><a href="wishlist.html" class="text-dark text-hover-primary position-relative">
          <svg class="svg-icon navbar-icon">
            <use xlink:href="#heart-1"> </use>
          </svg>
          <div class="navbar-icon-badge">3</div></a></li>
      <li class="list-inline-item position-relative mr-3"><a href="#" data-toggle="modal" data-target="#sidebarCart" class="text-dark text-hover-primary">
          <svg class="svg-icon navbar-icon">
            <use xlink:href="#retail-bag-1"> </use>
          </svg>
          <div class="navbar-icon-badge">5</div></a></li>
    </ul>
    <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">
      <img class="svg-icon navbar-icon" src="images/menu.svg">
    </button>
    <div id="navbarContent" class="collapse navbar-collapse">
      <ul class="navbar-nav mt-3 mt-lg-0" id="menu" style="background: black; opacity: 0.7">
        <li class="nav-item dropdown active"><a id="homeDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle text-light">DANH MỤC</a>
          <div aria-labelledby="homeDropdown" class="dropdown-menu" style="transform: translateY(30px);">
            <?php $point->loadCategory("select * from  categories")?>
          </div>
        </li>
        <li class="nav-item dropdown"><a id="shopDropdown" href="products.php" aria-haspopup="true" aria-expanded="false" class="nav-link text-light">SẢN PHẨM</a>
        </li>
        <li class="nav-item dropdown "><a id="iconsDropdown" href="#" aria-haspopup="true" aria-expanded="false" class="nav-link text-light">LIÊN HỆ</a>
        </li>
        <!-- Megamenu-->
        <li class="nav-item dropdown position-static"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle text-light">TIN TỨC</a>
          <div data-translate-x="-50%" class="dropdown-menu megamenu px-4 px-lg-5 py-lg-5" style="transform: translateX(-50%) translateY(30px);">
            <div class="row">
              <div class="col-lg-3 col-sm-6"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/blog/christopher-campbell-28571-unsplash.adf10dd5.jpg" alt="" class="img-fluid mb-3 d-none d-lg-block">
                <!-- Megamenu list-->
                <h6>Homepage</h6>
                <ul class="megamenu-list list-unstyled">
                  <li class="megamenu-list-item"><a href="index.html" class="megamenu-list-link">Home v.1   </a></li>
                  <li class="megamenu-list-item"><a href="index-2.html" class="megamenu-list-link">Home v.2   </a></li>
                </ul>
                <!-- Megamenu list-->
                <h6>Category</h6>
                <ul class="megamenu-list list-unstyled mb-lg-0">
                  <li class="megamenu-list-item"><a href="category-full.html" class="megamenu-list-link">Full width   </a></li>
                  <li class="megamenu-list-item"><a href="category-full-sidebar.html" class="megamenu-list-link">Full width with category menu   </a></li>
                  <li class="megamenu-list-item"><a href="category-big-products.html" class="megamenu-list-link">Full width with big products   </a></li>
                  <li class="megamenu-list-item"><a href="category-boxed.html" class="megamenu-list-link">Boxed   </a></li>
                  <li class="megamenu-list-item"><a href="category-sidebar.html" class="megamenu-list-link">Boxed &amp; sidebar   </a></li>
                  <li class="megamenu-list-item"><a href="category-categories.html" class="megamenu-list-link">Subcategories   </a></li>
                </ul>
              </div>
              <div class="col-lg-3 col-sm-6"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/blog/ian-dooley-347962-unsplash.108cdfff.jpg" alt="" class="img-fluid mb-3 d-none d-lg-block">
                <!-- Megamenu list-->
                <h6>Order process</h6>
                <ul class="megamenu-list list-unstyled">
                  <li class="megamenu-list-item"><a href="cart.html" class="megamenu-list-link">Shopping cart   </a></li>
                  <li class="megamenu-list-item"><a href="checkout.html" class="megamenu-list-link">Checkout   </a></li>
                  <li class="megamenu-list-item"><a href="checkout-confirmed.html" class="megamenu-list-link">Checkout - confirmed   </a></li>
                  <li class="megamenu-list-item"><a href="wishlist.html" class="megamenu-list-link">Wishlist   </a></li>
                </ul>
                <!-- Megamenu list-->
                <h6>Product</h6>
                <ul class="megamenu-list list-unstyled mb-lg-0">
                  <li class="megamenu-list-item"><a href="detail-1.html" class="megamenu-list-link">Product with sticky info   </a></li>
                  <li class="megamenu-list-item"><a href="detail-2.html" class="megamenu-list-link">Product with background   </a></li>
                  <li class="megamenu-list-item"><a href="detail-3.html" class="megamenu-list-link">Product standard    </a></li>
                </ul>
              </div>
              <div class="col-lg-3 col-sm-6"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/blog/ian-dooley-347942-unsplash.27d1967c.jpg" alt="" class="img-fluid mb-3 d-none d-lg-block">
                <!-- Megamenu list-->
                <h6>Blog</h6>
                <ul class="megamenu-list list-unstyled">
                  <li class="megamenu-list-item"><a href="blog.html" class="megamenu-list-link">Blog   </a></li>
                  <li class="megamenu-list-item"><a href="blog-masonry.html" class="megamenu-list-link">Blog - Masonry   </a></li>
                  <li class="megamenu-list-item"><a href="post.html" class="megamenu-list-link">Post   </a></li>
                </ul>
                <!-- Megamenu list-->
                <h6>Pages</h6>
                <ul class="megamenu-list list-unstyled mb-lg-0">
                  <li class="megamenu-list-item"><a href="contact.html" class="megamenu-list-link">Contact   </a></li>
                </ul>
                
              </div>
              <div class="col-lg-3 col-sm-6"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/blog/photo-1534126511673-b6899657816a.0a721385.jpg" alt="" class="img-fluid mb-3 d-none d-lg-block">
                <!-- Megamenu list-->
                <h6>Documentation</h6>
                <ul class="megamenu-list list-unstyled">
                  <li class="megamenu-list-item"><a href="docs/index.html" class="megamenu-list-link">Introduction   </a></li>
                  <li class="megamenu-list-item"><a href="docs/directory-structure.html" class="megamenu-list-link">Directory structure   </a></li>
                  <li class="megamenu-list-item"><a href="docs/gulp.html" class="megamenu-list-link">Gulp   </a></li>
                  <li class="megamenu-list-item"><a href="docs/customizing-css.html" class="megamenu-list-link">Customizing CSS   </a></li>
                  <li class="megamenu-list-item"><a href="docs/credits.html" class="megamenu-list-link">Credits   </a></li>
                  <li class="megamenu-list-item"><a href="docs/changelog.html" class="megamenu-list-link">Changelog   </a></li>
                </ul>
                <!-- Megamenu list-->
                <h6>Components</h6>
                <ul class="megamenu-list list-unstyled mb-lg-0">
                  <li class="megamenu-list-item"><a href="docs/components-bootstrap.html" class="megamenu-list-link">Bootstrap   </a></li>
                  <li class="megamenu-list-item"><a href="docs/components-theme.html" class="megamenu-list-link">Theme   </a></li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        <!-- /Megamenu end-->
      </ul>
      <form action="#" class="d-lg-flex ml-auto mr-lg-5 mr-xl-6 my-4 my-lg-0">
        <div class="input-group input-group-underlined">
          <input type="text" placeholder="Search" aria-label="Search" aria-describedby="button-search" class="form-control form-control-underlined pl-3">
          <div class="input-group-append ml-0">
            <button id="button-search" type="button" class="btn btn-underlined py-0"> 
              <img src="images/icon-search.svg"alt="" class="icon-search">
            </button>
          </div>
        </div>
      </form>
      <?php
        if(isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['password']) && isset($_SESSION['phanquyen']))
        {
          $name = $point->loadnameUser($_SESSION['id']);
          echo '<ul class="list-inline mb-0 d-none d-lg-block">
          <li class="list-inline-item mr-3">
              <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="images/user-circle-solid.svg" alt="" class="account">
                </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item">'.$name.'</a>                          
                <a class="dropdown-item" href="source/logout.php">Log out</a>                        
              </div>
            </div>                                        
          </li>
          <li class="list-inline-item mr-3"><a href="wishlist.html" class="text-dark text-hover-primary position-relative">
              <img src="images/heart-regular.svg" alt="" class="heart">
              <div class="navbar-icon-badge">3</div></a></li>
          <li class="list-inline-item position-relative mr-3"><a href="#" data-toggle="modal" data-target="#sidebarCart" class="text-dark text-hover-primary">
              <img src="images/cart-arrow-down-solid.svg" alt="" class="cart">
              <div class="navbar-icon-badge">5</div></a></li>
          <li class="list-inline-item"><a href="#" data-toggle="modal" data-target="#sidebarRight" class="text-dark text-hover-primary">
              <svg class="svg-icon navbar-icon">
                <use xlink:href="#menu-hamburger-1"> </use>
              </svg></a></li>
          </ul>';    
        }
        else 
        {                      
          echo '<li class="list-inline-item mr-3"><a href="#" data-toggle="modal" data-target="#signupModal" class="text-dark text-hover-primary">
          <button class="btn btn-primary" >Sign up</button>
          </a></li>';
          echo  '<li class="list-inline-item mr-3"><a href="#" data-toggle="modal" data-target="#loginModal" class="text-dark text-hover-primary">
          <button  class="btn btn-primary">Sign in</button>
          </a></li>';
        }
      ?>       
    </div>
  </nav>
</header>