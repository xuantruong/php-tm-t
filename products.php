
<?php
require_once("source/spcart.php");
session_start();
$shopping = new dbshopping();
			if(!empty($_REQUEST["action"])) {
				switch($_REQUEST["action"]) {
          case "add":
            if(isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['password']) && isset($_SESSION['phanquyen']))
            {
              include("source/login.php");
              $p = new users();
              $p->xacthuc($_SESSION['id'], $_SESSION['username'], $_SESSION['password'], $_SESSION['phanquyen']);
            }
            else
            {
              echo '<script language="javascript">
              alert("Vui lòng đăng nhập trước");
              </script>';
              echo '<script language="javascript">
                    window.location="index.php";
                    </script>';
            break;
            }
						if(!empty($_POST["quantity"])) {
							$productByCode = $shopping->runQuery("SELECT * FROM product WHERE id='" . $_GET["id"] . "'");
							$itemArray = array($productByCode[0]["id"]=>array('productname'=>$productByCode[0]["productname"], 'id'=>$productByCode[0]["id"], 'quantity'=>$_POST["quantity"], 'price'=>$productByCode[0]["price"], 'image'=>$productByCode[0]["image"]));

							if(!empty($_SESSION["cart_item"])) {
								if(in_array($productByCode[0]["id"],array_keys($_SESSION["cart_item"]))) {
									foreach($_SESSION["cart_item"] as $k => $v) {
											if($productByCode[0]["id"] == $k) {
												if(empty($_SESSION["cart_item"][$k]["quantity"])) {
													$_SESSION["cart_item"][$k]["quantity"] = 0;
												}
												$_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
											}
									}
								} else {
									$_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
								}
							} else {
								$_SESSION["cart_item"] = $itemArray;
							}
						}
					break;
					case "remove":
						if(!empty($_SESSION["cart_item"])) {
							foreach($_SESSION["cart_item"] as $k => $v) {
									if($_GET["code"] == $k)
										unset($_SESSION["cart_item"][$k]);				
									if(empty($_SESSION["cart_item"]))
										unset($_SESSION["cart_item"]);
							}
						}
					break;
					case "empty":
						unset($_SESSION["cart_item"]);
					break;	
				}
				}
?>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
   <link rel="stylesheet" href="css/edit.css">
   <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/varkala/1-0/vendor/aos/aos.css">
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/varkala/1-0/css/style.default.19ba8987.css" id="theme-stylesheet">
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/varkala/1-0/css/custom.0a822280.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
    <script src="jquery/dist/jquery.slim.min.js"></script>
    <script src="jquery/dist/jquery.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
	<link href="css/styleshop.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <?php include('header.php'); ?>  
        <div class="container-fluid px-lg-5 px-xl-7 py-6 mt-4">
            <div class="show-products mt-4">
              
            </div>
            <div class="row justify-content-between align-items-center mb-4">
              <div class="col-12 col-sm">
                <ul class="list-inline text-center text-sm-left mb-3 mb-sm-0"> 
                  <li class="list-inline-item"><a href="#" class="text-dark">All Products </a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Clothing </a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Bags</a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Shoes</a></li>
                  <li class="list-inline-item"><a href="#" class="text-muted text-hover-dark">Accessories</a></li>
                </ul>
              </div>
              <div class="col-12 col-sm-auto text-center"><a href="#" class="btn btn-link px-0">All products</a></div>
            </div>
				<!---shopping cart-->
        <?php
      include("source/shoppingcart.php");
      ?>
			<!--product-->
        <div class="row">
	  			<div id="product-grid">
        	<div class="txt-heading">Products</div>
        	<?php
        	$product_array = $shopping->runQuery("SELECT * FROM product ORDER BY id ASC");
        	if (!empty($product_array)) { 
	       	foreach($product_array as $key => $value){
        	?>
              <div class="product-item">
              <form method="post" action="products.php?action=add&id=<?php $product_array[$key]["id"]; ?>">
                    <div class="product-image"><img src="<?php echo $product_array[$key]["image"]; ?>"></div>
                    <div class="product-tile-footer">
                    <div class="product-title"><?php echo $product_array[$key]["productname"]; ?></div>
                    <div class="product-price"><?php echo "$".$product_array[$key]["price"]; ?></div>
                    <div class="cart-action">
                    <input type="submit" value="Add to Cart" class="btnAddAction" />
                      <input type="text" class="product-quantity" name="quantity" value="1" size="2" />
                      </div>
                    </div>
              </form>
          </div>
          <?php
            }
          }
          ?>
        </div>
			<!--/product-->
                   <!-- Quickview Modal    -->
                   <div id="quickView" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade quickview" style="">
              <div role="document" class="modal-dialog modal-xl">
                <div class="modal-content">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close close-absolute close-rotate">
                    <svg class="svg-icon w-3rem h-3rem svg-icon-light align-middle">
                      <use xlink:href="#close-1"> </use>
                    </svg>
                  </button>
                  <div class="modal-body quickview-body">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="detail-carousel">
                          <div class="product-badge badge badge-primary">Fresh</div>
                          <div class="product-badge badge badge-dark">Sale</div>
                          <div id="quickViewSlider" class="swiper-container quickview-slider">
                            <!-- Additional required wrapper-->
                            <div class="swiper-wrapper">
                              <!-- Slides-->
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-1-gray.1453e467.jpg" alt="Modern Jacket 1" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-2-gray.e4eff43a.jpg" alt="Modern Jacket 2" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-3-gray.21f669db.jpg" alt="Modern Jacket 3" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-4-gray.f1e52d09.jpg" alt="Modern Jacket 4" class="img-fluid"></div>
                              <div class="swiper-slide"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-5-gray.b7b1581e.jpg" alt="Modern Jacket 5" class="img-fluid"></div>
                            </div>
                          </div>
                          <div data-swiper="#quickViewSlider" class="swiper-thumbs">
                            <button class="swiper-thumb-item detail-thumb-item mb-3 active"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-1-gray.1453e467.jpg" alt="Modern Jacket 0" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-2-gray.e4eff43a.jpg" alt="Modern Jacket 1" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-3-gray.21f669db.jpg" alt="Modern Jacket 2" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-4-gray.f1e52d09.jpg" alt="Modern Jacket 3" class="img-fluid"></button>
                            <button class="swiper-thumb-item detail-thumb-item mb-3 0"><img src="https://d19m59y37dris4.cloudfront.net/varkala/1-0/img/product/detail-5-gray.b7b1581e.jpg" alt="Modern Jacket 4" class="img-fluid"></button>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6 p-lg-5">
                        <h2 class="mb-4 mt-4 mt-lg-1">Push-up Jeans</h2>
                        <div class="d-flex flex-column flex-sm-row align-items-sm-center justify-content-sm-between mb-4">
                          <ul class="list-inline mb-2 mb-sm-0">
                            <li class="list-inline-item h4 font-weight-light mb-0">$65.00</li>
                            <li class="list-inline-item text-muted font-weight-light"> 
                              <del>$90.00</del>
                            </li>
                          </ul>
                          <div class="d-flex align-items-center text-sm">
                            <ul class="list-inline mr-2 mb-0">
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-primary"></i></li>
                              <li class="list-inline-item mr-0"><i class="fa fa-star text-gray-300"></i></li>
                            </ul><span class="text-muted text-uppercase">25 reviews</span>
                          </div>
                        </div>
                        <p class="mb-4 text-muted">Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.</p>
                        <form id="buyForm_modal" action="#">
                          <div class="row">
                            <div class="col-sm-6 col-lg-12 detail-option mb-4">
                              <h6 class="detail-option-heading">Size <span>(required)</span></h6>
                              <div class="dropdown bootstrap-select"><select name="size" data-style="btn-selectpicker" class="selectpicker" tabindex="-98">
                                <option value="value_0">Small</option>
                                <option value="value_1">Medium</option>
                                <option value="value_2">Large</option>
                              </select><button type="button" class="btn dropdown-toggle btn-selectpicker" data-toggle="dropdown" role="combobox" aria-owns="bs-select-1" aria-haspopup="listbox" aria-expanded="false" title="Small"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner">Small</div></div> </div></button><div class="dropdown-menu "><div class="inner show" role="listbox" id="bs-select-1" tabindex="-1"><ul class="dropdown-menu inner show" role="presentation"></ul></div></div></div>
                            </div>
                            <div class="col-sm-6 col-lg-12 detail-option mb-5">
                              <h6 class="detail-option-heading">Type <span>(required)</span></h6>
                              <label for="material_0_modal" class="btn btn-sm btn-outline-primary detail-option-btn-label">
                                  
                                Hoodie
                                <input type="radio" name="material" value="value_0" id="material_0_modal" required="" class="input-invisible">
                              </label>
                              <label for="material_1_modal" class="btn btn-sm btn-outline-primary detail-option-btn-label">
                                  
                                College
                                <input type="radio" name="material" value="value_1" id="material_1_modal" required="" class="input-invisible">
                              </label>
                            </div>
                          </div>
                          <div class="input-group w-100 mb-4">
                            <input name="items" type="number" value="1" class="form-control form-control-lg detail-quantity">
                            <div class="input-group-append flex-grow-1">
                              <button type="submit" class="btn btn-dark btn-block"> <i class="fa fa-shopping-cart mr-2"></i>Add to Cart</button>
                            </div>
                          </div>
                          <div class="row mb-4">
                            <div class="col-6"><a href="#"> <i class="far fa-heart mr-2"></i>Add to wishlist </a></div>
                            <div class="col-6 text-right">
                              <ul class="list-inline mb-0">
                                <li class="list-inline-item mr-2"><a href="#" class="text-dark text-hover-primary"><i class="fab fa-facebook-f"> </i></a></li>
                                <li class="list-inline-item"><a href="#" class="text-dark text-hover-primary"><i class="fab fa-twitter"> </i></a></li>
                              </ul>
                            </div>
                          </div>
                          <ul class="list-unstyled">
                            <li><strong>Category:</strong> <a href="#" class="text-muted">Jeans</a></li>
                            <li><strong>Tags:</strong> <a href="#" class="text-muted">Leisure</a>, <a href="#" class="text-muted">Elegant</a></li>
                          </ul>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container py-6">
              <h5 class="text-uppercase text-primary letter-spacing-3 mb-3">Our History</h5>
              <p class="lead text-muted mb-4">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections </p>
              <p class="lead text-muted mb-5">He must have tried it a hundred times, shut his eyes so that he wouldn't have to look at the floundering legs, and only stopped when he began to feel a mild, dull pain there that he had never felt before.     </p>
            </div>
              <!---footer-->
              <?= include("footer.php"); ?>
    </body>
</html>