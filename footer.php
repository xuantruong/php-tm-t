<footer>
                  <!-- Services block-->
                  <!-- Main block - menus, subscribe form-->
                  <div class="py-6 text-muted" id="foot-block"> 
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-5 pr-lg-5 pr-xl-6 mb-5 mb-lg-0">
                          <h6 class="text-dark letter-spacing-1 mb-4">Be in touch</h6>
                          <p class="text-sm mb-3"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. At itaque temporibus.</p>
                          <form action="#" id="newsletter-form">
                            <div class="input-group input-group-underlined mb-3">
                              <input type="email" placeholder="Your Email Address" aria-label="Your Email Address" class="form-control form-control-underlined">
                              <div class="input-group-append ml-0">
                                <button type="button" class="btn btn-underlined text-gray-700 py-0"> 
                                  <svg class="svg-icon w-2rem h-2rem">
                                    <use xlink:href="#envelope-1"> </use>
                                  </svg>
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                        <div class="col-lg-7">      
                          <div class="row">                
                            <div class="col-lg-4"><a data-toggle="collapse" href="#footerMenu0" aria-expanded="false" aria-controls="footerMenu0" class="d-lg-none block-toggler my-3">Shop<span class="block-toggler-icon"></span></a>
                              <!-- Footer collapsible menu-->
                              <div id="footerMenu0" class="expand-lg collapse">
                                <h6 class="text-dark letter-spacing-1 mb-4 d-none d-lg-block">Shop</h6>
                                <ul class="list-unstyled text-sm pt-2 pt-lg-0">
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">For Women</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">For Men</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Stores</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Our Blog</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Shop</a></li>
                                </ul>
                              </div>
                            </div>
                            <div class="col-lg-4"><a data-toggle="collapse" href="#footerMenu1" aria-expanded="false" aria-controls="footerMenu1" class="d-lg-none block-toggler my-3">Company<span class="block-toggler-icon"></span></a>
                              <!-- Footer collapsible menu-->
                              <div id="footerMenu1" class="expand-lg collapse">
                                <h6 class="text-dark letter-spacing-1 mb-4 d-none d-lg-block">Company</h6>
                                <ul class="list-unstyled text-sm pt-2 pt-lg-0">
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Login</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Register</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Wishlist</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Our Products</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Checkouts</a></li>
                                </ul>
                              </div>
                            </div>
                            <div class="col-lg-4"><a data-toggle="collapse" href="#footerMenu2" aria-expanded="false" aria-controls="footerMenu2" class="d-lg-none block-toggler my-3">Your account<span class="block-toggler-icon"></span></a>
                              <!-- Footer collapsible menu-->
                              <div id="footerMenu2" class="expand-lg collapse">
                                <h6 class="text-dark letter-spacing-1 mb-4 d-none d-lg-block">Your account</h6>
                                <ul class="list-unstyled text-sm pt-2 pt-lg-0">
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Login</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Register</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Wishlist</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Our Products</a></li>
                                  <li class="mb-2"> <a href="#" class="text-muted text-hover-dark link-animated">Checkouts</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Copyright section of the footer-->
                  <div class="py-4 font-weight-light text-muted" id="foot-block-last">
                    <div class="container">
                      <div class="row align-items-center text-sm text-gray-500">
                        <div class="col-lg-4 text-center text-lg-left">
                          <p class="mb-lg-0">© 2019 Petshome.  All rights reserved.</p>
                        </div>
                        <div class="col-lg-8">
                          <ul class="list-inline mb-0 mt-2 mt-md-0 text-center text-lg-right">
                            <li class="list-inline-item"> <a href="#" class="text-reset">Terms &amp; Conditions </a></li>
                            <li class="list-inline-item"> <a href="#" class="text-reset">Privacy &amp; cookies </a></li>
                            <li class="list-inline-item"> <a href="#" class="text-reset">Accessibility </a></li>
                            <li class="list-inline-item"> <a href="#" class="text-reset">Customer Data Promise </a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </footer>