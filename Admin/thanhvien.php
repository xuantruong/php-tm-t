<?php
  session_start();
  if(isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['password']) && isset($_SESSION['phanquyen']))
  {
    include("../source/login.php");
    $p = new users();
    $p->xacthuc($_SESSION['id'], $_SESSION['username'], $_SESSION['password'], $_SESSION['phanquyen']);
  }
  else
  {
    echo '<script language="javascript">
    alert("Vui lòng đăng nhập trước");
    </script>';
    echo '<script language="javascript">
          window.location="../index.php";
          </script>';
  }
?>

<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="img/favicon.png" type="image/png">
	<title>Fashiop</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../vendors/linericon/style.css">
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link rel="stylesheet" href="../vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="../vendors/lightbox/simpleLightbox.css">
	<link rel="stylesheet" href="../vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="../vendors/animate-css/animate.css">
	<link rel="stylesheet" href="../vendors/jquery-ui/jquery-ui.css">
	<!-- main css -->
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/responsive.css">
</head>
<body>
		<!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="top_menu row m0">
			

		</div>
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h"href="../index.php">
						<img src="../images/logo.png" alt="">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
					 aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					
				</div>
			</nav>
		</div>
	</header>
</br>
</br>
</br>
</br>
</br>
	<div class="container">
		<div class="row">
			<div class="col-3">
				<aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title">Admin</h4>
                              <ul class="list cat-list">
                                <li>
                                    <a href="baidang.php" class="d-flex justify-content-between">
                                        <p>Quản Lý Sản Phẩm</p>
                                    </a>
                                </li>
                                 <li>
                                    <a href="thanhvien.php" class="d-flex justify-content-between">
                                        <p>Quản Lý Thành Viên</p>
                                    </a>
                                </li>
                                 <li>
                                    <a href="chamsoc.php" class="d-flex justify-content-between">
                                        <p>Chăm Sóc Khách Hàng</p>
                                    </a>
                                </li>
                                 <li>
                                    <a href="donhang.php" class="d-flex justify-content-between">
                                        <p>Quản Lý Đơn Hàng</p>
                                    </a>
                                </li>
                            </ul>
                            <div class="br"></div>
                        </aside>
			</div>
			<div class="col-8">
				<div class="col-md-12"><div class="bgc-white bd bdrs-3 p-20 mB-20"><h4 class="c-grey-900 mB-20">Quản lý thành viên</h4><table class="table"><thead><tr><th scope="col">#</th><th scope="col">Họ Tên</th><th scope="col">Địa Chỉ</th><th scope="col">Tài Khoản</th></tr></thead><tbody><tr><th scope="row">1</th><td>Mark</td><td>Otto</td><td>@mdo</td></tr><tr><th scope="row">2</th><td>Jacob</td><td>Thornton</td><td>@fat</td></tr><tr><th scope="row">3</th><td>Larry</td><td>the Bird</td><td>@twitter</td></tr></tbody></table></div></div>
			</div>
		</div>
	</div>
	<!--================Header Menu Area =================-->
	<script src="../js/jquery-3.2.1.min.js"></script>
	<script src="../js/popper.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/stellar.js"></script>
	<script src="../vendors/lightbox/simpleLightbox.min.js"></script>
	<script src="../vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="../vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="../vendors/isotope/isotope-min.js"></script>
	<script src="../vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="../js/jquery.ajaxchimp.min.js"></script>
	<script src="../vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="../vendors/flipclock/timer.js"></script>
	<script src="../vendors/counter-up/jquery.counterup.js"></script>
	<script src="../js/mail-script.js"></script>
	<script src="../js/theme.js"></script>
</body>

</html>