<?php
class dbshopping {
	private $host = "localhost";
	private $user = "root";
	private $password = "";
	private $database = "csdlweb";
	private $conn;
		
	function connectDB() 
	{
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		if (!$conn) {
		die("Khong the ket noi");
		}

		mysqli_query($conn,"SET NAMES UTF8");
		return $conn;
	}
	
		function __construct()
	{
		$this->conn = $this->connectDB();
	}

	function runQuery($query)
	{
		$result = mysqli_query($this->conn,$query);
		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}		
		if(!empty($resultset))
			return $resultset;
	}
	
	function numRows($query)
	{
		$result  = mysqli_query($this->conn,$query);
		$rowcount = mysqli_num_rows($result);
		return $rowcount;	
	}
}
?>